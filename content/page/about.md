---
title: About me
subtitle: Why you'd want to hire me
comments: false
---

I lead teams in the crafting of responsive web apps, native apps, and internal platforms by way of qualitative research, requirements gathering, persona development, content strategy, information architecture, interaction design, functional specification documentation, and overall experience strategy and design.

With a career younger than 10 years, I've worked as an Interaction/UX Designer at global product companies and agencies. The products, clients, and brands I've worked with include Roc Nation, WGSN (formerly Stylesight), Daiichi Sankyo (Lixiana), AbbVie (Humira, Duopa), Pfizer (Rebif), Mead Johnson (Enfamil), Sanofi (Multaq, Nasacort), Boehringer Ingelheim (Gilotrif) and Proctor & Gamble (Crest, Oral B).

I'm currently a Senior Experience Designer at PLB Medicus in NYC.

I acquired a Master of Science in Integrated Digital Media from the Brooklyn Experimental Media Center at NYU.

I have a proven track record of integrating my skills in installation art, motion graphics, game development, audio engineering, digital photography, film production, and 3D modeling/animation with my professional craft. Years ahead of the hype, I wrote my master's thesis on the purposeful application of augmented reality technology in consumer products.

I'm into ludology, clear Gameboys, sci-fi novels, old Tiger Electronics handhelds, premium magazines, and the Power Rangers.

[View resume](https://docs.google.com/document/d/1Ts5Ih4IcoOdjzDBQ_zKUT4NfCSuSBkDkUIwGP_PEZvM/edit?usp=sharing)
